compiler = {
  
  // compile ( file ) takes a Lithium file as input and outputs a minified Javascript file.
  compile : function(file){
    
    var currentDepth = 0,
  variable = function(typed, unitsd, valued, variabled){
    if(!unitsd && typed in ["int","integer","float","flt","cplx","complex"]){
      unitsd = 'units';
    }else if(!unitsd){
      unitsd = 'null';
    }
    if(!valued){
      valued = null;
    }
    var object = {
      type : typed,
      units : unitsd,
      value : valued,
      variables : variabled
    };
    return object;
  }
  blocks = [],
  declared = {},
  reserved = 'do if in for new try var else null this true void with break catch class cnst constant false thrown while require import public return switch default str string int integer extends private function fn'.split(' ');

      // Iterate through each line and transform text
      var lines = file.split('\n').map(function(line) {

        // # comments to //
        line = line.replace(/(['"].*)?#([^'"]*['"])?/, function($0, $1, $2){
          return ($1 && $2) ? $0 : '//';
        });

        // if line begins with a comment, return
        if (/^\/\//.test(line.trimLeft())){
          return line;
        }

        // Replace  a tab with two spaces.
        line = line.replace(/\t/g, '  ');

        if((((line.length - line.trimLeft().length) % 2) !== 0) && (line.length - line.trimLeft().length) > 0){
          line = line.slice(1);
        }

        var oldline = line;

        // setup 'class' constructors and inherit if necessary
        line = line.replace(/\bclass\b\s+([\w\$\.]+)(?:\s*(extends)\s*([\w\$\.]+))?/, function($0, $1, $2, $3) {
          if ($1 && $2 && $3) {
            return 'var ' + $1 + ' = ' + $3 + '.extend({';
            currentDepth++;
          } else if ($1) {
            return 'var '+ $1 +' = Class.extend({';
            currentDepth++;
          }else{
            return $0;
          }
        });

        // transform a while loop
        line = line.replace(/\bwhile\b\s+([^{}]+)(\{)?/, function($0, $1, $2) {
          if ($2) {
            return $0;
          } else if ($1) {
            return "while (" + $1 + ") {";
          }else{
            return line;
          }
        });

        // until loop
        line = line.replace(/\buntil\b\s+([^{}]+)(\{)?/, function($0, $1, $2) {
          if ($2){
            return $0;
          } else if ($1) {
            return 'while (!( ' + $1 + ' )) {';
          }else{
            return line;
          }
        });

        // Bracketless for in statement
        line = line.replace(/for\s+([\w$]+)\s+in\s+(\{.+\}|[\w\$]+)/, function($0, $1, $2) {
          return 'for(var ' + $1 + ' in ' + $2 + '){';
        });

        line = line.replace(/for\s+([\w\$]+)\s+(?:\bin\b)\s+([\w\$]+)/, function($0, $1, $2) {
          return 'for(var i=0,len='+$2+'.length;i<len;i++){ var '+$1+'='+$2+'[i];';
        });

        // Function shorthand, func, or func foo
        line = line.replace(/(\bfn\b)\s*([\w\$]+)?\s*(\([\w\$\s,]*\))?/, function($0, $1, $2, $3) {
          if ($1 && $2 && $3){
            //      var foo = function(a, b) {
            return 'var '+$2+'=function'+$3+'{';
          }else if ($1 && $2 && !$3){
            //      var   foo  = function() {
            return 'var '+$2+'=function(){';
          }else if ($1 && !$2 && $3){
            //      function   (a, b)  {
            return 'function' + $3 + '{';
          }else if ($1 && !$2 && !$3){
            return 'function(){';
          }else{
            throw new Error("Can not compile function");
          }
        });

        if(oldline !== line){
          return line;
        }

        // Alias `end` with `}` fakes a negative lookbehind for a word char
        line = line.replace(/(\w)?end(?!\w)/g, function($0, $1) {
          return $1 ? $0 : '}';
        });

        //Alias output
        line = line.replace(/output/, "console.log");

        // Inserts brackets into switch statement
        line = line.replace(/(switch) (.+)/, function($0, $1, $2) {
          return $1 + "(" + $2 + "){";
        });

        if(oldline !== line){
          return line;
        }

        // Replaces when with case. If then is found a one line statement is
        // assumed. This currently inserts a semicolon to prevent errors with
        // appended break.
        line = line.replace(/(case)\s(.+)/, function($0, $1) {
          if ($0) {
            return "case " + $1 + ":";
          }else{
            return line;
          }
        });

        // Replaces default with default: in a switch statement
        line = line.replace(/\bdefault\b(?!\:)/, function($0, $1) {
          // if entire line is just default
          if (line.trim() === 'default'){
            return 'default:';
          }else{
            return $0;
          }
        });

        // Replaces an `else if` with `} else if () {`
        line = line.replace(/else if (?!\s*\()(.+)/, function($0, $1) {
          if (insideString("else if", line)) return $0;
          return '}else if(' + $1 + '){';
        });

        // Replaces an `else` with `} else {`
        line = line.replace(/else(?!(?:\s*\{)| if)/, function($0) {
          // if `else` is inside a string, bail out
          if (insideString("else", line)) return $0;
          return '}else{';
        });

        // Replaces an `if condition` with `if (condition) {`
        line = line.replace(/if (?!\s*\()(.+)/, function($0, $1) {
          // if `if` is inside a string, bail out
          if (insideString("if", line)) return $0;
          return 'if(' + $1 + '){';
        });

        line = line.replace(/object ([\$\w]+)(?: < ([\$\w]+))?/, function($0, $1, $2) {
          if (insideString("object", line)) return $0;
          if ($1 && $2) {
            return 'var ' + $1 + '={ __proto__: ' + $2 + ',';
          } else {
            return 'var ' + $1 + '={';
          }
        });

        // Replaces `def foo.bar` methods with vanilla methods
        line = line.replace(/\bfn\b ([\$\w]+\.[\$\w]+)(\([\$\w, ]*\))?/, function($0, $1, $2) {
          if (insideString("fn", line)) return $0;
          // if def declaration has parens
          if ($2)
            return $1 + ' = function' + $2 + '{';
          else
            return $1 + ' = function(){';
        });

        // Replaces `def foo >> bar` methods with vanilla methods
        line = line.replace(/\bfn\b ([\$\w]+) prototype ([\$\w]+)(\([\$\w, ]*\))?/, function($0, $1, $2, $3) {
          if (insideString("def", line)){
            return $0;
          }
          if ($3){
            return $1 + '.prototype.' + $2 + ' = function' + $3 + ' {';
          }else{
            return $1 + '.prototype.' + $2 + ' = function() {';
          }
        });

        // Replaces `def` methods with vanilla methods inside of an object literal.
        line = line.replace(/obj ([\$\w]+)(?: inherits ([\$\w]+))?/, function($0, $1, $2) {
          if (insideString("fn", line)) return $0;
          // if def declaration has parens
          if ($2)
            return $1 + ': ' + 'function' + $2 + ' {';
          else
            return $1 + ': ' + 'function() {';
        });

        // Transform `parent*` to `__proto__`
        line = line.replace(/\bparent\b\*/, function() {
          return '__proto__';
        });

        if(line !== ''){
          var endofline = line.trimRight().slice(-1);
          if(endofline != '{' && endofline != "}" && endofline != ";" && endofline != ''){
            line += ";";
          }
        }else{
          return '';
        }
        return line;
      });
        return lines.join('');
  return '';
}

  },
  outputMode : "javascript"
}
